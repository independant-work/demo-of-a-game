# Demo of a game
Author: Jacob Jones
This project is a passion project done in spare time aiming eventually to make a physics based 2d game.

In the development before using common methods to achieve the desired effects I have tried to come up with working ideas before comparing them with online solutions in an attempt 
to further my own understanding and drive innovation and as such I have reworked most of the code several times as I try out and learn different things.

currently very rudimentary with many issues to resolve
-stable build is the last working as intended-ish build 
-active development is not garanteed to build and run and if it does will likely not show the desired physics.

Currently does:
-collision with axis aligned shapes on both x and y axis
-air resistance
-player interaction with velocity
-calculate physics realistically

To do:
-add surface friction
-change collision to line-line intersection to allow for rotated collision
-improve collision algorithm using chunks
-improve collision handling
