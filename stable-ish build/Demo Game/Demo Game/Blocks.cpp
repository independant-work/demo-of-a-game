#include "Blocks.h"
#include "Blocks.h"
#include "SFML/Graphics.hpp"
#include <iostream>
#include <stdlib.h>

using namespace std;

//makes a square using a vertex array 
void Blocks::makeSquare(float mx, float my) {

    shapes = sf::VertexArray(sf::Quads, 4);

    shapes[0].position = sf::Vector2f(20, 40);
    shapes[1].position = sf::Vector2f(40, 40);
    shapes[2].position = sf::Vector2f(40, 20);
    shapes[3].position = sf::Vector2f(20, 20);
}

//old bounce still in use in bounds so kept for now
void Blocks::bounce(char axis) {
    //should be force or velocity * elasticity of an object = velocity in rebound
    //should be in Blocks
    //should also account for x 
    float velocity;

    if (axis == 'x') {
        velocity = velocityX;
        flipVelDirX();
    }
    else
    {
        velocity = velocityY;
        flipVelDirY();
    }

    float velocityAfterBounce = 0;
    velocityAfterBounce = velocity * 0.3;

    if (velocityAfterBounce < 0.6) {
        velocityAfterBounce = 0;
        dogravity = false;
    }

    if (axis == 'x') {
        velocityX = velocityAfterBounce;
    }
    else
    {
        velocityY = velocityAfterBounce;
    }

}


//increments velocity by gravitys value
void Blocks::gravity() {
    //gravity applies a +9.8 m/s regardless of mass
    // so at 20 milliseconds its 0.196
    if (veldirY == velocityDirectionY::up) {
        velocityY = velocityY - 9.8;
        checkVelocityDirectionY();
    }
    else {
        velocityY = velocityY + 9.8;
    }
}

//
void Blocks::airResistance(char axis) {
    float velocity = 0;
    
    if (axis == 'x') {
        velocity = velocityX;

    }
    else {
        velocity = velocityY;
     
    }



    //neutons 2nd law f=m*g force = mass*acceleration 
    // which can also be force = velocity*mass / time frame
    //mass of a 1 meter cubed block of dirt is 1.5 tonnes 

    //if we are doing the calculation every 10 milliseconds it becomes
    
  
    
    float force = (velocity * mass) / 0.01 ;

    //force =  ((air density)(drag)(area in contact with air))/2)*velocity squared
    float airresistance = (0.5 * ((1.225 * 0.8 * 10) * (velocity * velocity)));
    // reducing the force of air resistance by 70% as it looked unnatural 
    airresistance = airresistance * 0.7;
    
    velocity = (force - airresistance)*0.01 / mass;


    if (axis == 'x') {
        velocityX= velocity;

    }
    else {
        velocityY = velocity;

    }



}

//checks that velocity is not a negative number
void Blocks::checkVelocityDirectionY() {
    if (velocityY < 0) {
        velocityY = 0 - velocityY;
        flipVelDirY();
    }
}
//checks that velocity is not a negative number
void Blocks::checkVelocityDirectionX() {
    if (velocityX < 0) {
        velocityX = 0 - velocityX;
        flipVelDirX();
    }
}

//gets the distance between two coordinates and determines the direction of movement
//returns a float value
float Blocks::getDistance(float oldCoords, float newCoords , char axis) {
    float distance;

    if (oldCoords > newCoords) {
        distance = oldCoords - newCoords;

        if (axis == 'x') {
           veldirX = velocityDirectionX::left;
        }
        else{
            veldirY = velocityDirectionY::up;
        }
    }
    else  {
        distance = newCoords - oldCoords;

        if (axis == 'x') {
            veldirX = velocityDirectionX::right;
        }
        else{
            veldirY = velocityDirectionY::down;
        }
    }
    return distance;
}

//moves the block object based on its direction and the distance passed to it
void Blocks::moveBlock(float distance, char axis) {

    if (axis == 'x') {
        if (veldirX == velocityDirectionX::right) {
            newX = x + distance;
        }
        else
        {
            newX = x - distance;
        }
    }
    else {
        if (veldirY == velocityDirectionY::down) {
            newY = y + distance;
        }
        else
        {
            newY = y - distance;
        }
    }

}

//calls multiple functions to calculate the forces affecting the block and
//then move the block based on the calculations
void Blocks::doPhysics(){

    if (dogravity) {
        gravity();
    }
    
    //calculates the air resistance for both axis
    airResistance('x');
    airResistance('y');

   
    
    //0.01 as 10 milliseconds, the cycle time

    //calculates the distance travelled in this cycle and moves the block by it
    float distanceX = velocityX * 0.01;
    moveBlock(distanceX, 'x');
    
    float distanceY = velocityY * 0.01;
    moveBlock(distanceY, 'y');

}


