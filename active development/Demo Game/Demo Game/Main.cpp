/*
Main
Author: Jacob Jones
Version: 0.13
*/

#include <iostream>
#include <string>
#include "SFML/Graphics.hpp"
#include "SFML/OpenGL.hpp"
#include "SFML/Window.hpp"
#include <String.h>
#include <vector>
#include <chrono>
#include "Blocks.h"
#include "CollisionGroup.h"


using namespace std;
using std::vector;
typedef std::chrono::high_resolution_clock Clock;

//creates an enum for use in the state machine
enum class State { loading, game, pause };

auto start = Clock::now();
auto inputClock = Clock::now();
auto clickInputClock = Clock::now();

int resolutionX = 800;
int resolutionY = 600;

//used to determine if its the first iteration of the loop
bool firstRun = true;
//a struct which is used to store the positions for objects in a vector
struct pairings
{
    int i;
    int k;
};

Blocks *activeBlock ;


sf::RenderWindow window(sf::VideoMode(resolutionX, resolutionY), "My window");


vector <Blocks> blockStorage;
vector<pairings> collidedStore;

State state{ State::loading };








//player tools///////////////////////////////////////////////////////

/*this method spawns a block when the mouse is clicked at its position*/
void spawnShapeOnClick(sf::Vector2i mouseposition) {
    auto current = Clock::now();
    double duration = std::chrono::duration_cast<std::chrono::milliseconds>(current - inputClock).count();

    // only spawns  a block if the elapsed time has passed to prevent blocks spawning in each other
    if (duration >= 250) {

        Blocks block;
        block.makeSquare(0, 0);
        block.setOrigin(30.f, 30.f);
        block.setPosition(mouseposition.x, mouseposition.y);        
        block.setOldX(mouseposition.x);
        block.setOldY(mouseposition.y);
        blockStorage.push_back(block);
        inputClock = Clock::now();
    }
}

/*the method clears all shapes in the window*/
void clearAllShapesOnClick() {
    blockStorage.clear();
}

/*clears the shapes in a square around the mouse*/
void clearShapes() {
    sf::Vector2i mouseposition = sf::Mouse::getPosition(window);
    float x = mouseposition.x;
    float y = mouseposition.y;
    //blockstorage[0].makeRed();
    
    for (int i = 0; i < blockStorage.size(); i++) {
        if (((blockStorage[i].getX() < x + 50) && (blockStorage[i].getX() > x - 50) &&
            (blockStorage[i].getY() < y + 50) && (blockStorage[i].getY() > y - 50))) {
            blockStorage.erase(blockStorage.begin() + i);
        }
    }
    

}


// sets the block in the same location as the mouse to active block
void setActiveBlock() {
    sf::Vector2i mouseposition = sf::Mouse::getPosition(window);

    for (int i = 0; i < blockStorage.size(); i++) {
        if ((blockStorage[i].getX() < mouseposition.x + 10 && (blockStorage[i].getX() > mouseposition.x - 10) &&
            (blockStorage[i].getY() < mouseposition.y + 10) && (blockStorage[i].getY() > mouseposition.y - 10))) {
            activeBlock = &blockStorage[i];
            activeBlock->setDoGravity(false);
        }

    }
}
//makes the active block follow the mouse
void followMouse() {
    sf::Vector2i mouseposition = sf::Mouse::getPosition(window);
    
    activeBlock->setOldX(activeBlock->getX());
    activeBlock->setOldY(activeBlock->getY());
    
    activeBlock->setPosition(mouseposition.x, mouseposition.y);
}
//calculates the resulting velocity after the mouse is released
void calcVelocity() {
    float distanceX =  activeBlock->getDistance( activeBlock->getOldX(), activeBlock->getX(), 'x');
    float distanceY = activeBlock->getDistance(activeBlock->getOldY(), activeBlock->getY(), 'y'); 
   
    activeBlock->setVelocityX(distanceX / 0.01);
    activeBlock->setVelocityY(distanceY / 0.01);
    activeBlock->checkVelocityDirectionY();

}



//pre game state which fills the available space with blocks
//currently not in use as collision algorithm too inefficient
void loadBlocks() {

    for (int i = 10; i < resolutionX; i = i + 20) {
        for (int k = 10; k < resolutionY; k = k + 20) {
            Blocks block;

            block.makeSquare(0, 0);

            block.setOrigin(30.f, 30.f);
            block.setPosition(i, k);

            /*
             
            block.setdophysics(false);
            block.setdogravity(false);
            block.setdocollisions(false);
            */
            blockStorage.push_back(block);
        }
    }


}


//updates the blocks stored location each cycle
void fillXY() {
    for (int i = 0; i < blockStorage.size(); i++) {
        blockStorage[i].fillXY(blockStorage[i].getPosition().x, blockStorage[i].getPosition().y);
    }
}



//collision detection stuff

/*
void splitWindow() {
    int boxheight = resolutiony / 10;
    int boxwidth = resolutionx / 10;

    int xcount = 0;
    int ycount = 0;
    while (xcount < resolutionx)
    {
        while (ycount < resolutiony) {
            CollisionGroup newGroup(boxheight, boxwidth, xcount, ycount);
            collisiongroups.push_back(newGroup);
            ycount = ycount + boxheight;

        }
        ycount = 0;
        CollisionGroup newGroup(boxheight, boxwidth, xcount, ycount);
        collisiongroups.push_back(newGroup);
        xcount = xcount + boxwidth;

    }

}*/


// calculates the resulting velocities of 2 colliding blocks
void improvedBounce(Blocks* i, Blocks* k, char axis) {
    //2 different equations to calculate final velocity after a collision on a line
    // final velocity of 1 = initial velocity 1 *(mass of 1 - mass of 2) + 2*mass of 2 * initial velocity of 2 / mass of 1 + mass of 2 
    // final velocity of 2 = initial velocity 2 *(mass of 2 - mass of 1) + 2*mass of 1 * initial velocity of 1 / mass of 1 + mass of 2 

    float initialVelocityI;
    float initialVelocityK;
    if (axis == 'x') {
        initialVelocityI = i->getVelocityX();
        initialVelocityK = k->getVelocityX();

        if ((i->getVelDirX() == 'r' && k->getVelDirX() == 'l') || (i->getVelDirX() == 'l' && k->getVelDirX() == 'r')) {
            i->flipVelDirX();
            k->flipVelDirX();
        }
    }
    else {
        initialVelocityI = i->getVelocityY();
        initialVelocityK = k->getVelocityY();

        if ((i->getVelDirY() == 'u' && k->getVelDirY() == 'd') || (i->getVelDirY() == 'd' && k->getVelDirY() == 'u')) {
            i->flipVelDirY();
            k->flipVelDirY();
        }
    }

    float finalVelocityI = initialVelocityI * (i->getMass() - k->getMass()) + 2 * k->getMass() * initialVelocityK / (i->getMass() + k->getMass());
    float finalVelocityK = initialVelocityK * (k->getMass() - i->getMass()) + 2 * i->getMass() * initialVelocityI / (i->getMass() + k->getMass());


    if (axis == 'x') {
        i->setVelocityX(finalVelocityI);
        k->setVelocityX(finalVelocityK);
        i->checkVelocityDirectionX();
        k->checkVelocityDirectionX();
    }
    else {
        i->setVelocityY(finalVelocityI);
        k->setVelocityY(finalVelocityK);
        i->checkVelocityDirectionY();
        k->checkVelocityDirectionY();
    }


}
// checks if the int given in negative and makes it positive if it is
float checkNegative(int i) {
    if (i < 0) {
        i = 0 - i;
    }
    return i;
}

// finds out if two blocks will collide next cycle and takes preventative measures if they will
// currently bounce is trigged the same cycle but will hopefully be moved to trigger next cycle soon
void predictMove(int i, int k) {

    int differenceX = blockStorage[i].getNewX() - blockStorage[k].getNewX();  
    differenceX = checkNegative(differenceX);

    int differenceY = blockStorage[i].getNewY() - blockStorage[k].getNewY();
    differenceY = checkNegative(differenceY);

    if ((differenceX <= 20)&&(differenceY <= 20)) {
        /*
        float distanceI = blockstorage[i].getnewx() - blockstorage[i].getx();
        distanceI = checkNegative(distanceI);
        float distanceK = blockstorage[k].getnewx() - blockstorage[k].getx();
        distanceK = checkNegative(distanceK);
       */

        //uses position in relation to the other block to determine which axis of the block is colliding
        if ((blockStorage[i].getNewX() >= blockStorage[k].getNewX() + 17) || (blockStorage[i].getNewX() <= blockStorage[k].getNewX() - 17)) {
            
            int moveSizeX = (20 - differenceX) / 2;
            moveSizeX = moveSizeX + 2;

            switch (blockStorage[i].getVelDirX())
            {
            case 'r':

                blockStorage[i].addToNewX(-moveSizeX);

                break;
            case 'l':
                blockStorage[i].addToNewX(moveSizeX);
                break;
            }
            switch (blockStorage[k].getVelDirX())
            {
            case 'r':
                blockStorage[k].addToNewX(-moveSizeX);
                break;
            case 'l':
                blockStorage[k].addToNewX(moveSizeX);
                break;
            }
            improvedBounce(&blockStorage[i], &blockStorage[k], 'x');
        }
        if ((blockStorage[i].getNewY() >= blockStorage[k].getNewY() + 17) || (blockStorage[i].getNewY() <= blockStorage[k].getNewY() - 17)) {
            int moveSizeY = (20 - differenceY) / 2;
            moveSizeY = moveSizeY + 2;

            switch (blockStorage[i].getVelDirY())
            {
            case 'u':

                blockStorage[i].addToNewY(moveSizeY);

                break;
            case 'd':
                blockStorage[i].addToNewY(-moveSizeY);
                break;
            }
            switch (blockStorage[k].getVelDirY())
            {
            case 'u':
                blockStorage[k].addToNewY(moveSizeY);
                break;
            case 'd':
                blockStorage[k].addToNewY(-moveSizeY);
                break;
            }

            improvedBounce(&blockStorage[i], &blockStorage[k], 'y');
        }
        
        // adds the colliding pair to the store so it dosent collide again this cycle
        pairings test;
        test.i = i;
        test.k = k;
        collidedStore.push_back(test);
       
        
    }
    
}



// checks for collisions using methods above
void collisionDetection(int i) {

    for (int k = 0; k < blockStorage.size(); k++) {
        if (k != i) {
            if (blockStorage[k].getDoCollisions()) {
                bool match = false;
                for (int j = 0; j < collidedStore.size(); j++) {

                    if ((collidedStore[j].i == i && collidedStore[j].k == k) || (collidedStore[j].i == k && collidedStore[j].k == i)) {
                        match = true;
                    }
                }
                if (!match) {
                    predictMove(i, k);
                    
                }
            }


        }
    }
}
   


//checks if a block has passed the bounds and bounces it back if it has
void boundsDetection(int i) {
    if (blockStorage[i].getNewY() > (resolutionY)) {

        float difference = blockStorage[i].getNewY() - resolutionY;
        blockStorage[i].setNewY(blockStorage[i].getNewY() - difference - 10);
        blockStorage[i].setVelocityY(0);
        blockStorage[i].setDoGravity(false);
        //blockstorage[i].setdophysics(false);
    }
   
    if (blockStorage[i].getNewX() > (resolutionX)) {

        float difference = blockStorage[i].getNewX() - resolutionX;
        blockStorage[i].setNewX(blockStorage[i].getNewX() - difference - 10);
        blockStorage[i].bounce('x');
        //blockstorage[i].setdophysics(false);
    }
    if (blockStorage[i].getNewY() < 0) {

        float difference = blockStorage[i].getNewY();
        blockStorage[i].setNewY(blockStorage[i].getNewY() - difference + 10);
        blockStorage[i].bounce('y');
        // blockstorage[i].setdophysics(false);
    }
    if (blockStorage[i].getNewX() < 0) {

        float difference = blockStorage[i].getNewX();
        blockStorage[i].setNewX(blockStorage[i].getNewX() - difference + 10);
        blockStorage[i].bounce('x');
        //  blockstorage[i].setdophysics(false);
    }
}


// physics n stuff

void physics() {

    for (int i = 0; i < blockStorage.size(); i++) {

        if (blockStorage[i].getDoPhysics() == true) {
            blockStorage[i].doPhysics();
        }

        else
        {
            blockStorage[i].setNewX(blockStorage[i].getX());
            blockStorage[i].setNewY(blockStorage[i].getY());
        }
        
        if (blockStorage[i].getDoCollisions() == true) {
            collisionDetection(i);
        }

        boundsDetection(i);

        blockStorage[i].setPosition(blockStorage[i].getNewX(), blockStorage[i].getNewY());

        
    }
    collidedStore.clear();
}





// checks user inputs and calls methods correspondingly
void input() {

    bool holding = false;

    switch (state)
    {
    case State::loading:


        if (!firstRun) {
            state = State::game;
        }
        firstRun = false;

        break;
    case State::game:

        if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {           
            if (activeBlock == nullptr) {
                setActiveBlock();
            }
            else
            {
                followMouse();
            }

        }
        else if (activeBlock != nullptr)
        {
            activeBlock->setDoPhysics(true);
            activeBlock->setDoGravity(true);
            calcVelocity();     
            activeBlock = nullptr;
        }

        if (sf::Mouse::isButtonPressed(sf::Mouse::Right)) {         
            clearShapes();
        }
        
        if (sf::Mouse::isButtonPressed(sf::Mouse::Middle)) {
            spawnShapeOnClick(sf::Mouse::getPosition(window));         
        } 

        break;
    case State::pause:

        break;
    default:

        break;
    }
}

// most code goes here it handles things like calculating physics and loading blocks in
void updates() {

    auto finnish = Clock::now();
    double duration = std::chrono::duration_cast<std::chrono::milliseconds>(finnish - start).count();

    switch (state)
    {
    case State::loading:

        //splitWindow();
        //loadblocks();       

        

        break;
    case State::game:

        if (duration >= 10) {



            fillXY();
            physics();


            start = Clock::now();
        }

        break;
    case State::pause:

        break;
    default:

        break;
    }

}

// handles rendering the objects to screen
void render() {
    window.clear(sf::Color::Black);
    state = State::game;

    switch (state)
    {
    case State::loading:

        break;
    case State::game:

        for (int k = 0; k < blockStorage.size(); k++) {
            window.draw(blockStorage[k]);
        }
        break;
    case State::pause:
        break;
    default:
        break;
    }

    window.display();
}



//main loop 
int main() {
    //enables v-sync
    window.setVerticalSyncEnabled(true);
    while (window.isOpen())
    {

        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        
        input();
        updates();
        render();

    }
    return 0;
};