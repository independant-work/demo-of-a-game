#pragma once
#include "SFML/Graphics.hpp"
#include <vector>

using std::vector;
class CollisionGroup
{
private:
	int boxheight = 0;
	int boxwidth = 0;

	int ycoord = 0;
	int xcoord = 0;

	vector <int> BlocksInRange;

public:

	CollisionGroup(int height, int width, int x, int y) {
		boxheight = height;
		boxwidth = width;
		ycoord = y;
		xcoord = x;
	}



	int getBoxHeight() { return boxheight; }
	void setBoxHeight(int input) { boxheight = input; }

	int getBoxWidth() { return boxwidth; }
	void setBoxWidth(int input) { boxwidth = input; }

	int getycoord() { return ycoord; }
	void setycoord(int input) { ycoord = input; }

	int getxcoord() { return xcoord; }
	void setxcoord(int input) { xcoord = input; }

	sf::Vector2i getBottomRightCorner() {
		sf::Vector2i bottomright = sf::Vector2i(xcoord + boxwidth, ycoord + boxheight);
		return bottomright;
	}
	sf::Vector2i getTopLeftCorner() {
		sf::Vector2i topleft = sf::Vector2i(xcoord, ycoord);
		return topleft;
	}

	void AddToVector(int i) { BlocksInRange.push_back(i); }
	void RemoveFromVector(int i) { }

};