/*
Blocks
Author: Jacob Jones
Version: 0.13
*/
#pragma once
#include "SFML/Graphics.hpp"
#include <iostream>
#include <stdlib.h>

using namespace std;

enum class velocityDirectionX { left, right, none};
enum class velocityDirectionY { up, down};

enum class blockShape {square, none };

class Blocks : public  sf::Drawable, public sf::Transformable
{
private:

    //physics values   
    velocityDirectionX veldirX = velocityDirectionX::none;
    velocityDirectionY veldirY = velocityDirectionY::down;
    blockShape shape = blockShape::none;

    float x = 0;
    float y = 0;

    float velocityY = 0;
    float velocityX = 0;
   
    float mass = 1500;
    float oldY = 0;
    float oldX = 0;

    float newY = 0;
    float newX = 0;

    bool dophysics = true;
    bool docollisions = true;
    bool dogravity = true;

public:

    //inline simple functions
    float getNewY() { return newY; }
    float getNewX() { return newX; }

    void setNewY(float input) { newY = input; }
    void setNewX(float input) { newX = input; }

    float getY() { return y; }
    float getX() { return x; }

    void fillXY(int input1, int input2) {
        x = input1;
        y = input2;
    }

    float getOldY() { return oldY; }
    float getOldX() { return oldX; }

    void setOldY(float val) { oldY = val; }
    void setOldX(float val) { oldX = val; }

    float getMass() { return mass; }

    bool getDoPhysics() { return dophysics; }
    void setDoPhysics(bool newval) { dophysics = newval; }

    bool getDoGravity() { return dogravity; }
    void setDoGravity(bool newval) { dogravity = newval; }

    bool getDoCollisions() { return docollisions; }
    void setDoCollisions(bool newval) { docollisions = newval; }



    void addToNewY(float input) { newY = newY + input; }
    void addToNewX(float input) { newX = newX + input; }

    float getVelocityY() { return velocityY; }
    void setVelocityY(float mx) { velocityY = mx; }
    

    float getVelocityX() { return velocityX; }
    void setVelocityX(float mx) { velocityX = mx; }
    

    void checkVelocityDirectionY();
    void checkVelocityDirectionX();

    
    //still simple functions assigning so kept inline
    void makeRed() {
        shapes[0].color = sf::Color::Red;
        shapes[1].color = sf::Color::Red;
        shapes[2].color = sf::Color::Red;
        shapes[3].color = sf::Color::Red;
    }
 
    char getVelDirX() {
        if (veldirX == velocityDirectionX::right) {
            return 'r';
        }
        else
        {
            return 'l';
        }
    }
    void setVelDirX(char i) {
        if (i == 'l') {
            veldirX = velocityDirectionX::left;
        }
        if (i == 'r') {
            veldirX = velocityDirectionX::right;
        }
    }
    char getVelDirY() {
        if (veldirY == velocityDirectionY::up) {
            return 'u';
        }
        else
        {
            return 'd';
        }
    }
    void setVelDirY(char i) {
        if (i == 'u') {
            veldirY == velocityDirectionY::up;
        }
        if (i == 'd') {
            veldirY == velocityDirectionY::down;
        }
    }

    void flipVelDirX() {
        if (veldirX == velocityDirectionX::right) {
            veldirX = velocityDirectionX::left;
        }
        else
        {
            veldirX = velocityDirectionX::right;
        }
    }

    void flipVelDirY() {
        if (veldirY == velocityDirectionY::down) {
            veldirY = velocityDirectionY::up;
        }
        else
        {
            veldirY = velocityDirectionY::down;
        }
    }



    //function definitions

    float getDistance(float oldCoords, float newCoords, char axis);
    void makeSquare(float mx, float my);
 

    //physics stuff

    void gravity();
    void airResistance(char xory);
    void doPhysics();
    void moveBlock(float distance, char axis);
    //kept as bounds still uses this version
    void bounce(char xory);


private:

    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const
    {
        states.transform *= getTransform();

        target.draw(shapes, states);
    }

    sf::VertexArray shapes;
};
